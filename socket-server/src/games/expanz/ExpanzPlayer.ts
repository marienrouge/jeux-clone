import { CafejeuxPlayer } from "./../../CafejeuxPlayer";
import { ExpanzTypes } from "./ExpanzTypes";
import { Vector2 } from "./../../Utils";

export class ExpanzPlayer extends CafejeuxPlayer
{
	public currentColor?:ExpanzTypes.Color;
	public initialPosition?:Vector2;
	public territory:Vector2[] = [];
	
	/**
	 * Check that the new territory annexed by the player is linked to its own territory.
	 * It is considered as linked if one of the positions passed as parameter is the initial position.
	 *
	 * @params pos:Vector2[] => The positions to check.
	 * @returns true if one of the position cross the first territory captured by player, false otherwise.
	 */
	public isTerritoryLinked(positions:Vector2[]):boolean
	{
		const p = this.initialPosition!;
		const output = positions.find((v) => {
			return (v.x === p.x && v.y === p.y);
		});
		
		return (output !== undefined);
	}
}