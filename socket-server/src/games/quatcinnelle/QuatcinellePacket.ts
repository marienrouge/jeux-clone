import { IReceivedPacket, ISendPacket, PlayerId } from "etwin-socket-server";
import { Vector2 } from "./../../Utils";

export namespace QuatcinellePackets
{
    export interface Play extends IReceivedPacket, ISendPacket
    {
        position:Vector2;
        owner:PlayerId;
    }
    export interface Reconnect extends ISendPacket
    {
        pawns:PlayerId[][];
    }
    export interface End extends ISendPacket
    {
        pawns:Vector2[];
        hasWon:boolean;
    }
};