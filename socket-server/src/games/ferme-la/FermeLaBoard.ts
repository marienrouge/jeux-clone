import { FermeLaTypes } from "./FermeLaTypes"; 
import { LinearMatrix, Vector2 } from "./../../Utils";

export class FermeLaBoard extends LinearMatrix<FermeLaTypes.Tile<boolean>>
{
    private static readonly LINKS_RATIO = 0.25;
    private static readonly NB_COWS = 2;
    private static readonly NB_SHEEPS = 4;
    
    constructor(size:number)
    {
        super(size);
        this.init();
        this.addAnimals(FermeLaTypes.Animals.COW, FermeLaBoard.NB_COWS);
        this.addAnimals(FermeLaTypes.Animals.SHEEP, FermeLaBoard.NB_SHEEPS);
    }
    /**
     * A link  is not considered as valid in two cases:
     * - The distance between them is not equals to 1 unit.
     * - They are in diagonal instead of being aside.
     *
     * @params p1:Vector2
     * @params p2:Vector2
     * @returns A boolean indicates whether or not the two points make a valid link.
     */
    private static isLinkValid(p1:Vector2, p2:Vector2):boolean
    {
        const d = {
            x: Math.abs(Math.sign(p1.x - p2.x)),
            y: Math.abs(Math.sign(p1.y - p2.y))
        };

        return ((d.x === 1 && d.y === 0) || (d.x === 0 && d.y === 1));
    }
    public static isAroundPoint(p1:Vector2, p2:Vector2):boolean
    {
        return (Math.abs(p1.x - p2.x) <= 1 && Math.abs(p1.y - p2.y) <= 1);
    }
    public static getNearestPointFromZero(p1:Vector2, p2:Vector2):Vector2
    {
        return ((p1.x < p2.x || p1.y < p2.y) ? p1 : p2);
    }
    
    private init()
    {
        this.forEach((d, x, y) => {
            const links = {
                x: x < this.getWidth() - 1 && Math.random() < FermeLaBoard.LINKS_RATIO,
                y: y < this.getHeight() - 1 && Math.random() < FermeLaBoard.LINKS_RATIO
            };

            this.set(x, y, links);
            if (this.hasMadeGlobalSquares({x, y}).length > 0)
                this.set(x, y, {x: false, y: false, animal: FermeLaTypes.Animals.DEFAULT});
            d;
        });
    }
    private addAnimals(type:FermeLaTypes.Animals, nb:number)
    {
        const pos = {x: 0, y: 0};
        
        for (let i = nb; i > 0; i--) {
            pos.x = ~~(Math.random() * (this.getWidth() - 1));
            pos.y = ~~(Math.random() * (this.getHeight() - 1));
            this.get(pos)!.animal = type;
        }
    }
    /**
     * Check if there is a square from the position given as paramater.
     * The position passed as paramater is considered as the top-left corner of the square.
     *
     * @params pos:Vector = The top-left corner of the square to be checked.
     * @returns The position passed as parameter if there is a square, undefined otherwise.
     */
    private hasMadeLocalSquare(pos:Vector2):Vector2|undefined
    {
        const cur = this.get(pos);
        const sides:Vector2<boolean|undefined> = {
            x: this.get(pos.x, pos.y + 1)?.x,
            y: this.get(pos.x + 1, pos.y)?.y
        };

        if (cur?.x == true && cur?.y == true && sides.x == true && sides.y == true)
            return (pos);
        return (undefined);
    }
    private isOutOfBounds(pos:Vector2):boolean
    {
        return (pos.x >= this.getWidth() || pos.x < 0 || pos.y >= this.getHeight() || pos.y < 0);
    }
    /**
     * Add a new link between the two points passed as parameter.
     *
     * @params p1:Vector2 = The first point of the link.
     * @params p2:Vector2 = The second point of the link.
     * @returns True if the segment is a new segment, false if it has already been set to true.
     */
    public setLink(p1:Vector2, p2:Vector2):boolean
    {
        const inter = this.get(FermeLaBoard.getNearestPointFromZero(p1, p2));

        if (this.isOutOfBounds(p1) || this.isOutOfBounds(p2))
            return (false);
        if (!inter || !FermeLaBoard.isLinkValid(p1, p2))
            return (false);
        if (inter.x && inter.y)
            return (false);
        inter.x = inter.x || p1.x - p2.x !== 0;
        inter.y = inter.y || p1.y - p2.y !== 0;
        return (true);
    }
    /**
     * Returns all existing squares that were made around the point passed as parameter.
     *
     * @params pos:Vector2 = The position to be checked.
     * @returns The position of the top-left corner of every square around, or undefined.
     */
    public hasMadeGlobalSquares(pos:Vector2):Vector2[]
    {
        const output = [
            this.hasMadeLocalSquare(pos),
            this.hasMadeLocalSquare({x: pos.x - 1, y: pos.y}),
            this.hasMadeLocalSquare({x: pos.x, y: pos.y - 1})
        ];
        
        return (<Vector2[]>output.filter((p) => p !== undefined));
    }
    /**
     * Returns true if all possible links were made, false otherwise.
     *
     * @returns A boolean indicates whether or not all links were filled.
     */
    public isFilled():boolean
    {
        let cur;
        
        for (let x = this.getWidth() - 1; x >= 0; x--) {
            for (let y = this.getHeight() - 1; y >= 0; y--) {
                cur = this.get(x, y);
                if ((!cur?.x && x + 1 < this.getWidth()) 
                || (!cur?.y && y + 1 < this.getHeight()))
                    return (false);
            }
        }
        return (true);
    }
    /**
     * Convert every virtual link as an array of physical links.
     * Each cell of the array is composed of another array containing two Vector2.
     * Theses two points are a "physical" link.
     *
     * @returns An array of links (where each link is an array of two Vectors).
     */
    public convertToLinks():Array<Vector2[]>
    {
        const output:Array<Vector2[]> = [];

        this.forEach((d, x, y) => {
            if (d?.x)
                output.push([{x, y}, {x: x + 1, y}]);
            if (d?.y)
                output.push([{x, y}, {x, y: y + 1}]);
        });
        return (output);
    }
    public getAnimals():FermeLaTypes.Tile<number>[]
    {
        const output:FermeLaTypes.Tile<number>[] = [];

        this.forEach((d, x, y) => {
            if (d?.animal)
                output.push({x, y, animal: d?.animal});
        });
        return (output);
    }
    public set(x:number, y:number, v:FermeLaTypes.Tile<boolean>)
    {
        if (v.animal === undefined) {
            v.animal = this.get(x, y)?.animal;
        }
        super.set(x, y, v);
    }
}