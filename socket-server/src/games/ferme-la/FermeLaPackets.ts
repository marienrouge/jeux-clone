import { IReceivedPacket, ISendPacket, PlayerId } from "etwin-socket-server";
import { FermeLaTypes } from "./FermeLaTypes";
import { Vector2 } from "./../../Utils";

export module FermeLaPackets
{
    export interface Start extends ISendPacket
    {
        animals:Array<FermeLaTypes.Tile<number>>;
        links:Array<Vector2[]>;
        width:number;
        height:number;
    }
    export interface Update extends ISendPacket
    {
        link:Vector2[];
        score:number;
        squares:Vector2[];
        owner:PlayerId;
    }
    export interface End extends ISendPacket
    {
        hasWon:boolean;
    }
    export interface Play extends IReceivedPacket
    {
        start:Vector2;
        end:Vector2;
    }
}