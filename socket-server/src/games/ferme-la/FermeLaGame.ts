import { ACafejeuxGame } from "./../../ACafejeuxGame";
import { FermeLaBoard } from "./FermeLaBoard";
import { FermeLaPackets } from "./FermeLaPackets";
import { FermeLaPlayer } from "./FermeLaPlayer";
import { Vector2 } from "./../../Utils";

export class FermeLaGame extends ACafejeuxGame<FermeLaPlayer>
{
    private static readonly SIZE = 7;
    private readonly intersections = new FermeLaBoard(FermeLaGame.SIZE);
    
    constructor(opts:any)
    {
        super(opts);
        this.registerReceiveEvent("play", this.playTurn);
    }
    
    private playTurn = (data:FermeLaPackets.Play, emitter:FermeLaPlayer) =>
    {
        let squares:Vector2[];
 
        if (!data.start || !data.end || !this.checkTurnIntegrity(emitter.ID))
            return;
        if (!this.intersections.setLink(data.start, data.end))
            return;
        squares = this.getSquaresFromThisLink(data.start, data.end);
        emitter.score += squares.length;
        this.broadcast<FermeLaPackets.Update>("update", {
            link: [data.start, data.end],
            squares: squares,
            score: emitter.score,
            owner: emitter.ID
        });
        if (squares.length === 0)
            this.changeTurn();
        if (this.intersections.isFilled())
            this.stop();
    }
    /**
     * Returns the squares that were newly created with the link passed as parameter.
     *
     * @params start:Vector2
     * @params end:Vector2
     * @returns An array containing every new square made from the link.
     */
    private getSquaresFromThisLink(start:Vector2, end:Vector2):Vector2[]
    {
        const nearestFromZero = FermeLaBoard.getNearestPointFromZero(start, end);
        let output = this.intersections.hasMadeGlobalSquares(nearestFromZero);

        output = output.filter((v) => {
            return (FermeLaBoard.isAroundPoint(start, v) 
                && FermeLaBoard.isAroundPoint(end, v));
        });
        return (output);
    }
    
	protected getDataOnReconnection = () =>
	{
		//TO DO
		return ({});
	}
    
    public run()
    {
        this.broadcast<FermeLaPackets.Start>("start", {
            animals: this.intersections.getAnimals(),
            links: this.intersections.convertToLinks(),
            width: FermeLaGame.SIZE,
            height: FermeLaGame.SIZE
        });
    }
    public close()
    {
        let highestScore = 0;

        this.apply((p) => {
            if (p.score > highestScore)
                highestScore = p.score;
        });
        this.apply((p) => {
            p.send<FermeLaPackets.End>("end", {
                hasWon: p.score === highestScore
            });
        });
    }
}