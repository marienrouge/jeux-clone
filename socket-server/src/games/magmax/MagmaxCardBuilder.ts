import * as Card from "./MagmaxCards";
import { MagmaxBoard } from "./MagmaxBoard";
import { MagmaxGame } from "./MagmaxGame";
import { MagmaxPlayer } from "./MagmaxPlayer";

type CardCtor = new (type:string, board:MagmaxBoard, player:MagmaxPlayer) => Card.AMagmaxCard;

type CardType = {
    ratio:number, 
    ctor:CardCtor;
};

export class MagmaxCardBuilder
{
    private static readonly CARDS = new Map<string, CardType>([
        ["move_single", {ratio: 1, ctor: Card.MoveSingle}],
        ["shoot_water", {ratio: 0.6, ctor: Card.ShootWater}],
        ["move_all", {ratio: 0.4, ctor: Card.MoveAll}],
        ["shoot_laser", {ratio: 0.25, ctor: Card.ShootLaser}],
        ["place_bomb", {ratio: 0.15, ctor: Card.PlaceBomb}],
        ["place_rock", {ratio: 0.15, ctor: Card.PlaceRock}],
        ["protect_single", {ratio: 0.15, ctor: Card.Protect}],
        ["show_bombs", {ratio: 0.1, ctor: Card.ShowBombs}],
        ["slide_single", {ratio: 0.1, ctor: Card.Slide}],
        ["shoot_light", {ratio: 0.05, ctor: Card.ShootLight}]
    ]);
    private ponderationTotal = 0;
    public player?:MagmaxPlayer;

    constructor(private readonly board:MagmaxBoard)
    {
        MagmaxCardBuilder.CARDS.forEach((ct:CardType) => {
            this.ponderationTotal += ct.ratio;
        });
    }

    private generate(type:string):Card.AMagmaxCard|undefined
    {
        const cardData = MagmaxCardBuilder.CARDS.get(type);

        if (!cardData || !this.player)
            return (undefined);
        return (new cardData.ctor(type, this.board, this.player));
    }
    public generateHand():Card.AMagmaxCard[]
    {
        const output:Card.AMagmaxCard[] = [];

        for (let i = 0; i < MagmaxGame.DECK_SIZE; i++) {
            output.push(this.getCard());
        }
        return (output);
    }
    public getCard():Card.AMagmaxCard
    {
        const RND = Math.random() * this.ponderationTotal;
        let limit = 0;
        let type:string = "";

        MagmaxCardBuilder.CARDS.forEach((ct:CardType, t:string) => {
            if (limit < RND) {
                limit += ct.ratio;
                type = t;
            }
        });
        return (this.generate(type)!);
    }
}