import { CafejeuxPlayer } from "./../../CafejeuxPlayer";
import { AMagmaxCard } from "./MagmaxCards";
import { MagmaxBoard } from "./MagmaxBoard";
import { MagmaxPawn } from "./MagmaxPawn";

export class MagmaxPlayer extends CafejeuxPlayer
{
    public deck:AMagmaxCard[] = [];
    public pawns:MagmaxPawn[] = [];
    private _color = "";

    /**
     * Check that the card passed as parameter is inside the player's deck.
     *
     * @params id:string = The id of the card to be tested.
     * @returns The picked card if it exists, undefined otherwise.
     */
    public pickCard(id:string):AMagmaxCard|undefined
    {
        for (let i = this.deck.length - 1; i >= 0; i--) {
            if (this.deck[i].type === id) {
                return (this.deck.splice(i, 1)[0]);
            }
        }
        return (undefined);
    }

    /**
     * Check that the player has always at least a pawn alive on the board.
     *
     * @params board:MagmaxBoard = The board on which player pawns will be found.
     * @returns True if there is still at least a player's pawn on the board, false otherwise.
     */
    public isAlive(board:MagmaxBoard):boolean
    {
        for (let x = board.getWidth() - 1; x >= 0; x--) {
            for (let y = board.getHeight() - 1; y >= 0; y--) {
                if (board.get(x, y)?.pawn?.owner === this.ID)
                    return (true);
            }
        }
        return (false)
    }
    public getDeck():string[]
    {
        return (this.deck.map((card) => card.type));
    }
    public get canSeeBombs():boolean
    {
        for (let i = this.pawns.length - 1; i >= 0; i--) {
            if (this.pawns[i].seeBombs)
                return (true);
        }
        return (false);
    }
    public set color(color:string)
    {
        if (!this._color)
            this._color = color;
    }
}