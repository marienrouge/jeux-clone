import { PlayerId } from "etwin-socket-server";
import { Vector2 } from "./../../Utils";

export module HordesTypes
{
    export interface Tile
    {
        quantity:number;
        owner?:PlayerId;
    }
    export interface ModifiedTile extends Tile
    {
        position:Vector2;
    }
}