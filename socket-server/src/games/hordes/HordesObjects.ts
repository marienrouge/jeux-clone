import { HordesBoard } from "./HordesBoard";
import { HordesPlayer } from "./HordesPlayer";
import { Vector2 } from "./../../Utils";

export abstract class AHordesObject
{
    constructor(
        public readonly ID:string,
        protected readonly board:HordesBoard, 
        protected readonly player:HordesPlayer
    )
    {}
    
    /**
     * Override this method to code your object's logic.
     * It must returns undefined in case of fail, an array (even empty) in case of success.
     */
    public abstract execute(position:Vector2):Vector2[]|undefined;
}

export class Armageddon extends AHordesObject
{
    constructor(board:HordesBoard, player:HordesPlayer)
    {
        super("armageddon", board, player);
    }
    
    public execute(position:Vector2):Vector2[]|undefined
    {
        const tile = this.board.get(position);

        if (!tile || tile.owner === this.player.ID)
            return (undefined);
        this.player.consumeShot();
        this.board.set(position.x, position.y, undefined);
        return ([position]);
    }
}
export class BigCuteCat extends AHordesObject
{
    constructor(board:HordesBoard, player:HordesPlayer)
    {
        super("big_cute_cat", board, player);
    }
    
    public execute(position:Vector2):Vector2[]|undefined
    {
        const QUANTITY = 2 + ~~(Math.random() * 3);

        if (this.board.get(position)?.owner === this.player.ID)
            return (undefined);
        if (!this.board.addQuantityToTile(position, -QUANTITY))
            return (undefined)
        return ([position]);
    }
}
export class CallHelp extends AHordesObject
{
    constructor(board:HordesBoard, player:HordesPlayer)
    {
        super("call_help", board, player);
    }
   
    public execute(position:Vector2):Vector2[]|undefined
    {
        const tile = this.board.get(position);

        if (!tile || tile.owner !== this.player.ID)
            return (undefined);
        if (!this.board.addQuantityToTile(position, 1))
            return (undefined);
        return ([position]);
    }
}
export class MachineGun extends AHordesObject
{
    constructor(board:HordesBoard, player:HordesPlayer)
    {
        super("machine_gun", board, player);
    }
    
    public execute(position:Vector2):Vector2[]|undefined
    {
        const tile = this.board.get(position);
        const positionsAround = this.board.getTilesAround(position);
        const output:Vector2[] = [position];
        let collateralDamage:Vector2;

        if (!tile || tile.owner === this.player.ID || tile.quantity === 0)
            return (undefined);
        this.player.consumeShot();
        for (let i = 9; i > 0; i--) {
            if (Math.random() < 0.6)
                this.board.addQuantityToTile(position, -1);
            else {
                collateralDamage = positionsAround[~~(Math.random() * positionsAround.length)];
                output.push(collateralDamage);
                this.board.addQuantityToTile(collateralDamage, -1);
            }
        }
        return (output.filter((pos, index) => output.indexOf(pos) === index));
    }
}
export class RustyGun extends AHordesObject
{
    constructor(board:HordesBoard, player:HordesPlayer)
    {
        super("rusty_gun", board, player);
    }
    
    public execute(position:Vector2):Vector2[]|undefined
    {
        if (this.board.get(position)?.owner === this.player.ID)
            return (undefined);
        this.board.addQuantityToTile(position, -1)
        return ([position]);
    }
}
export class SmallHanging extends AHordesObject
{
    constructor(board:HordesBoard, player:HordesPlayer)
    {
        super("small_hanging", board, player);
    }

    public execute():Vector2[]|undefined
    {
        this.player.playShot();
        this.player.addShot();
        return ([]);
    }
}
export class ViciousTrap extends AHordesObject
{
    constructor(board:HordesBoard, player:HordesPlayer)
    {
        super("vicious_trap", board, player);
    }

    public execute(position:Vector2):Vector2[]|undefined
    {
        if (!this.board.addTrap(position))
            return (undefined);
        this.player.addShot();
        return ([]);
    }
}
export class WaterRation extends AHordesObject
{
    constructor(board:HordesBoard, player:HordesPlayer)
    {
        super("water_ration", board, player);
    }

    public execute():Vector2[]|undefined
    {
        this.player.addShot();
        return ([]);
    }
}