import { PlayerId } from "etwin-socket-server";
import { ACafejeuxGame } from "./../../ACafejeuxGame";
import { CafejeuxPlayer } from "./../../CafejeuxPlayer";
import { AmoniteBoard } from "./AmoniteBoard";
import { AmonitePackets } from "./AmonitePacket";

export class AmoniteGame extends ACafejeuxGame<CafejeuxPlayer>
{
    private readonly board = new AmoniteBoard();
    private readonly colors = ["red", "yellow"];
    private readonly winnerIds:PlayerId[] = [];

    constructor(opts:any)
    {
        super(opts);
        this.registerReceiveEvent("play", this.playTurn);
    }

    private playTurn = (data:AmonitePackets.Play, emitter:CafejeuxPlayer) =>
    {
        if (!this.checkTurnIntegrity(emitter.ID) || !data.position || !data.target)
            return;
        if (!this.board.isPawnOwnedByPlayer(data.position, emitter.ID))
            return;
        if (!this.board.isMoveValid(data.position, data.target))
            return;
        this.board.movePawn(data.position, data.target);
        this.broadcast<AmonitePackets.Update>("update", {
            position: data.position,
            target: data.target,
            id: emitter.ID
        });
        this.apply((p) => {
            if (this.board.areAllPawnsLinked(p.ID)) {
                this.winnerIds.push(p.ID);
            }
        });
        if (this.winnerIds.length > 0) {
            this.stop();
            return;
        }
        this.changeTurn();
    }

	protected getDataOnReconnection = () =>
	{
		//TO DO
		return ({});
	}
    
    public run()
    {
        this.apply((p) => this.board.registerPlayerId(p.ID));
        this.board.generatePawns();
        this.apply((p) => {
            p.send<AmonitePackets.Start>("start", {
                minHeight: AmoniteBoard.MIN_HEIGHT,
                maxHeight: AmoniteBoard.MAX_HEIGHT,
                color: this.colors.shift()!,
                board: this.board.getData()
            });
        });
    }
    public close()
    {
        this.apply((p) => {
            for (let i = this.winnerIds.length - 1; i >= 0; i--) {
                if (p.ID === this.winnerIds[i]) {
                    p.send<AmonitePackets.End>("end", {hasWon: true});
                    return;
                }
            }
            p.send<AmonitePackets.End>("end", {hasWon: false});
        });
    }
}