import { Vector2 } from "./../../Utils";

export module TrifusTypes
{
	export type Color = string|undefined
	export enum Element
	{
		START = 0,
		FIRE = 1,
		WATER = 2,
		WOOD = 3,
		COUNT = 4
	}
	export interface Card
	{
		strength:number;
		element:Element;
		type:string;
		owner:Color;
	}
	export interface PlacedCard
	{
		position:Vector2;
		card:Card;
	}
	export function toString(element:Element):string|undefined
	{
		const elements:any = {
			fire: Element.FIRE,
			water: Element.WATER,
			wood: Element.WOOD,
		};

		for (let key in elements) {
			if (element === elements[key])
				return (key);
		}
		return (undefined);
	}
}