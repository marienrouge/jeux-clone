import { PlayerId, ISendPacket, IReceivedPacket } from "etwin-socket-server";

export module CafejeuxPackets
{
    export interface Join extends ISendPacket
    {
        player:PlayerId;
    }
    export interface Leave extends ISendPacket
    {
        player:PlayerId;
    }
    export interface MessagePacket extends IReceivedPacket, ISendPacket
    {
        player:PlayerId;
        message:string;
    }
    export interface ChangeTurnPacket extends ISendPacket
    {
        current:PlayerId;
        remainingTime:number;
    }
}