# Magmax Socket Protocol

This file describe every event you can send/receive from client-side.

## Packet sent from server to client-side

EVERY packet of this side also has a senderId attribute, which let you identify your users.

### Start event

Event name: **start**

Description:\
This event is triggered when the game starts.

Content:
* color: string
* board: LinearMatrix<MagmaxTypes.Tile>
* deck: MagmaxTypes.Card[]

### Update board

Event name: **update**

Description:\
This event is triggered when a player's move is valid.\
It returns the updated board and deck.
Board may be undefined if the played card doesn't have any effect on it.

Content:
* board?: LinearMatrix<MagmaxTypes.Tile>
* deck: MagmaxTypes.Card[]

### Next turn event

Event name: **change_turn**

Description:\
This event is sent when the turn changes, always after the **update** event.\

Content:
* current:PlayerId
* remainingTime:number

### Game ended event

Event name: **end**

Description:\
This event is sent when the game is ended.\

Content:
* hasWon: boolean

## Packet sent from client to server-side

### Send move event

Event name: **play**

Description:\
Select the position of the pawn to be played, and the card to be played.\
Also, an orientation must be given for the move/slide cards.\
Note: To change turn, send an empty packet with this event.\
*Nothing will happen in the following cases:*
* *It is not the player's turn.*
* *The given position doesn't point to any player's pawn.*
* *The played card isn't in the player's hand.*
* *An orientation was required but not set.*
* *The movement is not rejected by the given card.*

Content:
* position: {x: number, y: number}
* card: MagmaxTypes.Card
* orientation: MagmaxTypes.Orientation|undefined

## Objects

### PlayerId

PlayerId is a simple string.\

### MagmaxTypes.Orientation

The orientation type is a restricted string, to theses four values:\
```"north"|"east"|"south"|"west"```

### MagmaxTypes.Pawn

Content:

* ID: number
* owner: PlayerId
* orientation: MagmaxTypes.Orientation
* seeBombs: boolean
* isProtected: boolean

### MagmaxTypes.Tile

Contains the state of the tile.\
If the player has not the ability to see bombs, the hasBomb property will always be false.\

Content:
* hasBomb: boolean
* hasRock: boolean
* pawn: MagmaxTypes.Pawn|undefined

### LinearMatrix<MagmaxTypes.Tile>

A linear matrix is a complex object, it is a multidimentional array optimized to fit in a single-dimention array.\
It is possible to reconstitute a bidimentional array by splitting the array in subarray of length "*width*", or access a tile at (x, y) with the following operation: ```y * width + x```.

Content:
* data: Array<MagmaxTypes.Tile>
* width: number
* height: number

### MagmaxTypes.Card

A card is a string restricted and describing a command.
Every action is named the same way: &ltverb&gt_&ltscope&gt
Probability of each card to spawn in its hand is indicated between parenthesis, but it is purely informational. Descriptions are also purely informational.
Here are all the possible values:
```"move_single"|"shoot_water"|"move_all"|"shoot_laser"|"place_bomb"|"place_rock"|"protect"|"show_bombs"|"slide_single"|"shoot_light"```

#### Moves/Slides (Orientation required):
* **move_single (10/30)**: Move a single player's pawn in the choosen orientation.
* **move_all (4/30)**: Move all player's pawns in the choosen orientation.
* **slide_single (1/30)**: "Slide" a player's pawn until it reach a border, a rock or a bomb.

All of these actions may be rejected if the pawn moved from 0 tiles.
All of these actions can also "push" a pawn and make it move (or slide).

#### Shoots:
* **shoot_water (6/30)**: If it reaches a pawn or a rock, it destroys it. If the pawn has a "protected" state, it loses its protection.
* **shoot_laser (2.5/30)**: Same as a shoot_water, but the shot may refracted in another direction if the pawn has a "protected" state.
* **shot_light (0.5/30)**: Destroy every rock and pawn in front of the pawn until it reaches a border. It can be refracted in another direction if a pawn in the shot has the protected state.

All of these actions will never be rejected if the "play" even is correctly filled.
All of theses actions doesn't need the orientation parameter because the shot is automately oriented using the orientation of the most recently pawn's move.

#### Pawn's state modifications:

* **show_bombs (1/30)**: Despites its name, it has two effects: It shows the bombs on the board, and it also shows if an adversary pawn has a "isProtected" or "seeBombs" state.
* **protect_single (1.5/30)**: Add a protection on a pawn, it can have various effects on shots.

#### Placements:
* **place_rock (1.5/30)**: Add a rock in front of a pawn.
* **place_bomb (1.5/30)**: Add a bomb under the pawn.
