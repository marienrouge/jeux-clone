# Amonite Socket Protocol

This file describe every event you can send/receive from client-side.

## Packet sent from server to client-side

EVERY packet of this side also has a senderId attribute, which let you identify your users.

### Start event

Event name: **start**

Description:\
This event is triggered when the game starts.
The board is an hexagon, and every column has a different height.
For this reason, there aren't width/height variables, but minHeight/maxHeight variables.\
minHeight is the height of the most little columns, so the left and right columns.\
maxHeight is the height of the largest column, so the column to the center of the board.\
A color variable is also sent, it represents the color of the current player.

Content:
* minHeight: number
* maxHeight: number
* board: Array<Array<string|undefined>>
* color: string

### Update board

Event name: **update**

Description:\
This event is triggered when a player's move is valid.\
It returns the position of the pawn that was moved, and its destination (target).
You must update the board you got with the start event with theses data.

Content:
* position: {x:number, y:number}
* target: {x:number, y:number}

### Next turn event

Event name: **change_turn**

Description:\
This event is sent when the next turn occurs, always after the **update** event.

Content:
* current: PlayerId
* remainingTime: number

### Game ended event

Event name: **end**

Description:\
This event is sent when the game is ended.\
The game is considerated as ended when a player has joined all of its pawns.\

Content:
* hasWon: boolean

## Packet sent from client to server-side

### Send move event

Event name: **play**

Description:\
Send the selected pawn and its destination on the board.\
*Nothing will happen in the following cases:*
* *It is not the player's turn.*
* *The pawn goes outside the map.*
* *The pawn's destination override a player's pawn.*
* *The move is not valid.*

Content:
* position: {x:number, y:number}
* target: {x:number, y:number}

## Objects

### PlayerId

PlayerId is a simple string.