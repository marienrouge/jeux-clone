import React from "react";

interface PlayerData
{
    value:number;
    color:string;
    remainingTime:number;
    isPlaying:boolean;
};

interface GameContextData
{
    data:PlayerData[];
    setRemainingTime:(t:number, target:Player) => void;
}

export enum Player
{
    MAIN,
    ADVERSARY
}

export const GameContext:React.Context<GameContextData> = React.createContext({}) as React.Context<any>;

export default class GameContextProvider extends React.Component
{
    state = {
        data: [{
                value: 0,
                color: "white",
                remainingTime: 5 * 60,
                isPlaying: false
            },
            {
                value: 0,
                color: "white",
                remainingTime: 5 * 60,
                isPlaying: false
            }
        ]
    };
    
    setRemainingTime = (time:number, target:Player) =>
    {
        const data = [...this.state.data];
        const userData = {...data[target]};
        
        userData.remainingTime = time;
        data[target] = userData;
        this.setState({data: data});
    }
   
    render()
    {
        return (
            <GameContext.Provider value={{
                ...this.state, 
                setRemainingTime: this.setRemainingTime
            }}>
                {this.props.children}
            </GameContext.Provider>
        );
    }
}