import React from "react";
import "./NavbarIcon.css";
import StylesDictionary from "./../generics/StylesDictionary";

interface NavbarIconProps
{
    url:string;
    route?:string;
    text:string;
    isMain?:boolean;
    onClick?:() => void;
}

export default function NavbarIcon(props:NavbarIconProps)
{
    let route = props.route || process.env.PUBLIC_URL + "/img/icons/menu/";
    
    return (
        <div
            className={`link menuIcon ${props.isMain ? "mainMenuIcon" : ""}`}
            onClick={props.onClick}
        >
            <img
                src={`${route}${props.url}`}
                alt={props.text}
                style={{display: "block", margin: "auto"}}
            />
            <div className="drop-caps" style={styles.text}>{props.text}</div>
        </div>
    );
}

const styles:StylesDictionary = {
    text: {
        textAlign: "center"
    }
};