import React from "react";
import { useSounds } from "./../hooks/useSounds";
import { GameContext, Player } from "./../contexts/GameContext";
import PlayerInfos from "./PlayerInfos";
import StylesDictionary from "./../generics/StylesDictionary";

interface ChatProps
{
    socket:SocketIOClient.Socket;
}

export default function Chat(props:ChatProps)
{
    const gameContext = React.useContext(GameContext);
    const [isActive, setIsActive] = React.useState(false);
    const [messages, setMessages] = React.useState([]) as [string[], Function];
    const [inputText, setInputText] = React.useState("");
    const [playSound] = useSounds(["my_turn"]);

    React.useEffect(() => {
        props.socket.on("join", (data:any) => {
            const message = `${data.emitter} a rejoint le jeu!`;

            setMessages(messages.concat([message]));
        });
        props.socket.on("message", (data:any) => {
            const message = `${data.emitter}: ${data.message}`;
            
            setMessages(messages.concat([message]));
        });
        props.socket.on("change_turn", (data:any) => {
            const active = data.senderId === data.current;
            const player = active ? Player.MAIN : Player.ADVERSARY;

            if (active)
                playSound("my_turn");
            setIsActive(active);
            gameContext.setRemainingTime(data.remainingTime, player);
        });
        return () => {
            props.socket.off("join");
            props.socket.off("message");
            props.socket.off("change_turn");
        }
    }, [props.socket, messages, setMessages, isActive, setIsActive]);
    
    const sendMessage = (e:React.FormEvent) =>
    {
        e.preventDefault();
        if (inputText.length < 2)
            return;
        props.socket.emit("message", {
            message: inputText
        });
        setInputText("");
    }
    const showMessages = () =>
    {
        return (
            messages.map((m, i) => {
                return (
                    <div key={i} style={styles.message}>{m}</div>
                );
            })
        );
    }
    
    return (
        <div style={styles.container}>
            <div style={styles.chatHeader}>
                <PlayerInfos
                    username="Volts"
                    level={1}
                    xp={50}
                    remainingTime={gameContext.data[Player.MAIN].remainingTime}
                    active={isActive}
                />
                <PlayerInfos
                    username="Natsu"
                    level={20}
                    xp={2000}
                    remainingTime={gameContext.data[Player.ADVERSARY].remainingTime}
                    maxXp={3000}
                    reverse={true}
                    active={!isActive}
                />
            </div>
            <div>
                <div style={styles.messagesContainer}>
                    {showMessages()}
                </div>
                <form autoComplete="off" onSubmit={sendMessage} style={styles.form}>
                    <input 
                        type="text"
                        onChange={(e) => setInputText(e.target.value)}
                        value={inputText}
                        style={styles.input}
                    />
                    <button>Envoyer</button>
                </form>
            </div>
        </div>
    );
}

const styles:StylesDictionary = {
    container: {
        display: "flex",
        flexDirection: "column",
        justifyContent: "space-between",
        height: process.env.REACT_APP_GAME_HEIGHT + "px" || 400,
    },
    chatHeader: {
        display: "flex",
        justifyContent: "space-between",
        marginBottom: 10
    },
    messagesContainer: {
        overflowY: "auto",
        maxHeight: process.env.REACT_APP_GAME_HEIGHT + "px" || 400, // Temporary
        marginBottom: 10
    },
    message: {
        color: "white",
        fontSize: "1em"
    },
    form: {
        display: "flex"
    },
    input: {
        flex: 1,
        display: "inline-block"
    }
};