import React from "react";
import { useHistory } from "react-router-dom";
import "./GameBox.css";
import BackgroundImageContainer from "./../generics/BackgroundImageContainer";
import StylesDictionary from "./../generics/StylesDictionary";

interface GameBoxProps
{
    name:string, 
    img:string, 
    diffucilty:string
};

export default function GameBox(props:GameBoxProps)
{
    const history = useHistory();

    const onClick = () =>
    {
        history.push(`/games/${props.name}`);
    }
    return (
        <div className="container" onClick={onClick}>
            <BackgroundImageContainer 
                url="games/box.gif"
                style={styles.box}
            >
                <BackgroundImageContainer
                    url={`games/label_${props.diffucilty}.gif`}
                    style={styles.diffuciltyLabel}
                >
                </BackgroundImageContainer>
                <BackgroundImageContainer
                    url={`games/${props.img}`}
                    style={styles.image}
                >
                    <div className="shadow">
                    </div>
                </BackgroundImageContainer>
            </BackgroundImageContainer>
        </div>
    );
}

const styles:StylesDictionary = {
    box: {
        margin: 10,
        position: "relative",
        width: 210,
        height: 210,
        boxShadow: "1px 1px 5px black",
        cursor: "pointer"
    },
    image: {
        margin: "auto",
        position: "relative",
        top: 5,
        width: 200,
        height: 180,
        borderRadius: 5,
    },
    diffuciltyLabel: {
        position: "absolute",
        width: "33%",
        height: "33%",
        zIndex: 2
    }
};