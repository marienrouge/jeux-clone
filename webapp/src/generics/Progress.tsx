import React from "react";
import StylesDictionary from "./StylesDictionary";

interface ProgressProps
{
    value:number;
    width?:number;
    text?:string|number;
    max?:number;
    reverse?:boolean;
}

export default function Progress(props:ProgressProps)
{
    return (
        <div style={{...styles.container, flexDirection: props.reverse ? "row-reverse" : "row"}}>
            <progress
                value={props.value}
                max={props.max || 100}
                style={{...styles.progress, ...{width: props.width || 110}}}
            />
            <span style={styles.text}>
                {props.text || props.value}
            </span>
        </div>
    );
}

const styles:StylesDictionary = {
    container: {
        display: "flex",
        alignItems: "center"
    },
    progress: {
        verticalAlign: "middle",
        marginRight: 2,
        margin: "0 4px"
    },
    text: {
        color: "rgb(240, 240, 0)",
        fontWeight: "bold",
        fontSize: "0.9em"
    }
};