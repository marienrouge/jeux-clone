import React from "react";

interface BackgroundImageContainerProps
{
    url:string;
    route?:string;
    noCover?:boolean;
    children:JSX.Element|JSX.Element[]|never[];
    style?:React.CSSProperties;
}

export default function BackgroundImageContainer(props:BackgroundImageContainerProps)
{
    let route = props.route || process.env.PUBLIC_URL + "/img/";
    
    return (
        <div style={{...props.style, ...{
            backgroundImage: `url(${route}${props.url})`,
            backgroundSize: props.noCover ? props.style?.backgroundSize : "cover"
        }}}>
            {props.children}
        </div>
    );
}