import { AGameSystem } from "./../AGameSystem";
import { FermeLaTile } from "./FermeLaTile"; 
import { LinearMatrix } from "./../lib/LinearMatrix";
import { Vector2 } from "./../lib/Vector2";

export class FermeLaSystem extends AGameSystem
{
    public readonly grid:LinearMatrix<FermeLaTile>;
    public readonly links:Array<Vector2[]> = [];
    
    constructor(socket:SocketIOClient.Socket, data:any)
    {
        super(socket, data);
        this.grid = new LinearMatrix(data);
        this.links = data.links;
        data.animals.forEach((a:any) => {
            this.grid.set(a.x, a.y, {animal: a.animal});
        });
    }

    private removeLink(start:Vector2, end:Vector2)
    {
        this.links.forEach((link, i) => {
            if (link[0].x === start.x && link[0].y === start.y
            && link[1].x === end.x && link[1].y === end.y) {
                this.links.splice(i, 1);
            }
        });
    }
    public update(data:any)
    {
        const colorAngle = data.senderId === data.owner ? 0 : 180;
        let tile:FermeLaTile|undefined;

        this.links.push(data.link);
        data.squares.forEach((square:Vector2) => {
            tile = {...this.grid.get(square.x, square.y), colorAngle};
            this.grid.set(square.x, square.y, tile);
        });
        for (let x = this.width - 2; x >= 0; x--) {
            for (let y = this.height - 2; y >= 0; y--) {
                tile = this.grid.get(x, y);
                if (tile?.colorAngle === undefined)
                    continue;
                if (tile.colorAngle === this.grid.get(x + 1, y)?.colorAngle)
                    this.removeLink({x: x + 1, y}, {x: x + 1, y: y + 1});
                if (tile.colorAngle === this.grid.get(x, y + 1)?.colorAngle)
                    this.removeLink({x, y: y + 1}, {x: x + 1, y: y + 1});
            }
        }
    }
    public end(data:any)
    {
        console.log(data);
    }
    public get width()
    {
        return (this.grid.WIDTH);
    }
    public get height()
    {
        return (this.grid.HEIGHT);
    }
}