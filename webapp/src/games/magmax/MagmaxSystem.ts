import { AGameSystem } from "./../AGameSystem";
import { LinearMatrix } from "./../lib/LinearMatrix";
import { MagmaxPawn } from "./MagmaxPawn";
import { MagmaxTile } from "./MagmaxTile";
import { Vector2 } from "./../lib/Vector2";

export class MagmaxSystem extends AGameSystem
{
    private _board:LinearMatrix<MagmaxTile|undefined>;
    private _deck:string[] = [];
    private readonly _pawns = new Map<number, MagmaxPawn>();
    public readonly color:"red"|"blue";

    constructor(socket:SocketIOClient.Socket, data:any)
    {
        super(socket, data);
        this._deck = data.deck;
        this.color = data.color;
        this._board = new LinearMatrix<MagmaxTile|undefined>(data.board);
        this.updatePawns(data.senderId);
    }
    
    private isPawnInMap(id:number):boolean
    {
        let output = false;
        
        this._board.forEach((tile) => {
            if (tile?.pawn?.ID === id)
                output = true;
        });
        return (output);
    }
    private updatePawns(senderId:string)
    {
        this._pawns.forEach((pawn) => pawn.isDead = !this.isPawnInMap(pawn.ID));
        this._board.forEach((tile, x, y) => {
            const p = tile?.pawn;
            let pawn:MagmaxPawn|undefined;

            if (!p)
                return;
            pawn = this._pawns.get(p.ID);
            if (pawn) {
                pawn.position = {x, y};
                pawn.isProtected = p.isProtected;
                pawn.seeBombs = p.seeBombs;
                return;
            }
            p.isOwned = p.owner === senderId;
            p.color = p.isOwned ? this.color : (this.color === "red" ? "blue" : "red");
            p.isSelected = (p.isOwned && x === ~~(this.board.WIDTH / 2));
            p.position = {x, y};
            this._pawns.set(p.ID, p);
        });
    }
    public update(data:any)
    {
        if (data.board) {
            this._board = new LinearMatrix<MagmaxTile|undefined>(data.board);
            this.updatePawns(data.senderId);
        }
        this._deck = data.deck;
    }
    public end(data:any)
    {
        
    }
    public updateSelectedPawn(pos:Vector2)
    {
        const isPositionInside = this.pawns.find((p) => {
            return (p.isOwned && p.position.x === pos.x && p.position.y === pos.y);
        });
        
        if (!isPositionInside)
            return;
        this._pawns.forEach((p) => {
            p.isSelected = (p.isOwned && pos.x === p.position.x && pos.y === p.position.y);
        });
    }
    public get board():LinearMatrix<MagmaxTile|undefined>
    {
        return (this._board);
    }
    public get deck():string[]
    {
        return (this._deck);
    }
    public get pawns():MagmaxPawn[]
    {
        return (Array.from(this._pawns.values()));
    }
}