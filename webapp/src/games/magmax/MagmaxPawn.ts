import { MagmaxOrientation } from "./MagmaxOrientation";
import { Vector2 } from "./../lib/Vector2";

export interface MagmaxPawn
{
    ID:number;
    owner:string;
    position:Vector2;
    orientation:MagmaxOrientation;
    isDead:boolean;
    seeBombs:boolean;
    isProtected:boolean;
    isOwned:boolean;
    color:string;
    isSelected:boolean;
}