import { Vector2 } from "./../lib/Vector2";

export class MagmaxBoard
{
    public position:Vector2 = {x: 0, y: 0};
    public size:Vector2 = {x: 0, y: 0};
    public scale:Vector2 = {x: 0, y: 0};

    public convertRelativePosition(pos:Vector2):Vector2
    {
        return ({
            x: this.position.x + pos.x * this.scale.x,
            y: this.position.y + pos.y * this.scale.y
        });
    }
    public convertAbsolutePosition(pos:Vector2):Vector2
    {
        return ({
            x: ~~((pos.x - this.position.x) / this.scale.x),
            y: ~~((pos.y - this.position.y) / this.scale.y)
        });
    }
}