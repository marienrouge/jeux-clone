import { AGame } from "./../AGame";
import { MagmaxDrawer } from "./MagmaxDrawer";
import { MagmaxPawn } from "./MagmaxPawn";
import { MagmaxSystem } from "./MagmaxSystem";
import { Mouse } from "./../Mouse";

export class MagmaxGame extends AGame<MagmaxSystem, MagmaxDrawer>
{
    constructor(socket:SocketIOClient.Socket, data:any)
    {
        super(MagmaxSystem, MagmaxDrawer, socket, data);
        this.helper.init(data.board.width, data.board.height, data.color);
        Mouse.onClick(() => {
            const id = this.helper.getCardIdFromMouse();
            const orientation = this.helper.getCardOrientation();
            let selected:MagmaxPawn|undefined;

            this.system.updateSelectedPawn(this.helper.getMousePosition());
            selected = this.system.pawns.find((p) => p.isSelected);
            if (id === -1 || !orientation || !selected)
                return;
            if (id === this.system.deck.length) {
                this.system.emit("play", {});
                return;
            }
            this.system.emit("play", {
                card: this.system.deck[id],
                position: selected.position,
                orientation: orientation
            });
        });
    }

    public draw(ctx:CanvasRenderingContext2D)
    {
        ctx.fillStyle = "rgb(254, 176, 4)";
        ctx.fillRect(0, 0, MagmaxGame.WIDTH, MagmaxGame.HEIGHT);
        this.helper.drawBackground();
        this.system.board.forEach((tile, x, y) => {
            if (!tile)
                return;
            this.helper.drawTile(tile, {x, y});
        });
        this.helper.drawPawns(this.system.pawns);
        this.helper.drawDeck(this.system.deck);
    }
}