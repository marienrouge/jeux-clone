export abstract class Mouse
{
    private static c:HTMLElement;
    public static x:number = 0;
    public static y:number = 0;

    public static init()
    {
        Mouse.c = document.getElementsByTagName("canvas")[0];
        window.addEventListener("mousemove", (e) => {
            const rect = Mouse.c.getBoundingClientRect();

            Mouse.x = e.clientX - rect.left;
            Mouse.y = e.clientY - rect.top;
        });
    }
    public static onClick(callback:Function)
    {
        Mouse.c.addEventListener("mousedown", () => {
            callback();
        })
    }
}