export const TimingFunctions = Object.freeze({
    LINEAR: (x:number) => x,
    EASE_IN: (x:number) => x * x,
    EASE_OUT: (x:number) => (2 - x) * x
});

interface AnimationParameters
{
    duration:number;
    repeatCount?:number;
    fill?:number;
    timingFunction?:(x:number) => number;
}

export class AnimatedValue
{
    private readonly DURATION:number;
    private readonly REPEAT_COUNT:number;
    private readonly FILL:number;
    private readonly timingFunction:(x:number) => number;
    private _value = 0;

    constructor(params:AnimationParameters)
    {
        this.DURATION = params.duration;
        this.REPEAT_COUNT = params.repeatCount || Infinity;
        this.FILL = params.fill || 1;
        this.timingFunction = params.timingFunction || TimingFunctions.LINEAR;
    }
    
    public update(d:number)
    {
        if (this._value < 1)
            this._value += d / this.DURATION;
        else
            this._value = this.FILL;
    }

    public get value():number
    {
        return (this.timingFunction(this._value));
    }
}