import { AShape } from "./AShape";
import { Triangle } from "./Triangle";

export class Rectangle extends AShape
{
    private _triangles:Triangle[] = [];
    
    constructor(
        private _x:number = 0, 
        private _y:number = 0, 
        private _w:number = 0, 
        private _h:number = 0,
        private _c:number = 0)
    {
        super();
        this.generateTriangles();
    }

    private generateTriangles()
    {
        const center = {x: this.x + this.w / 2, y: this.y + this.h / 2};
        const bottomRight = {x: this.x + this.w, y: this.y + this.h};

        this._triangles = [
            new Triangle({x: this.x, y: this.y}, center, {x: bottomRight.x, y: this.y}),
            new Triangle({x: this.x, y: this.y}, center, {x: this.x, y: bottomRight.y}),
            new Triangle({x: bottomRight.x, y: this.y}, center, bottomRight),
            new Triangle({x: this.x, y: bottomRight.y}, center, bottomRight)
        ];
    }
    
    public clone():Rectangle
    {
        return (new Rectangle(this.x, this.y, this.w, this.h, this._c));
    }
    public isPointInside(x:number, y:number):boolean
    {
        return (x >= this.x && x <= this.x + this.w 
            && y >= this.y && y <= this.y + this.h);
    }
    public draw(ctx:CanvasRenderingContext2D, isFilled?:boolean)
    {
        const W = this.x + this.w;
        const H = this.y + this.h;
        
        ctx.beginPath();
        ctx.moveTo(this.x + this._c, this.y);
        ctx.quadraticCurveTo(this.x, this.y, this.x, this.y + this._c);
        ctx.lineTo(this.x, H - this._c);
        ctx.quadraticCurveTo(this.x, H, this.x + this._c, H);
        ctx.lineTo(W - this._c, H);
        ctx.quadraticCurveTo(W, H, W, H - this._c);
        ctx.lineTo(W, this.y + this._c);
        ctx.quadraticCurveTo(W, this.y, W - this._c, this.y);
        ctx.closePath();
        super.draw(ctx, isFilled);
    }
    public drawImage(ctx:CanvasRenderingContext2D, image?:HTMLImageElement)
    {
        if (image)
            ctx.drawImage(image, this.x, this.y, this.w, this.h);
    }
    public scale(sx:number, sy = sx):Rectangle
    {
        this.x += (this.w - this.w * sx) / 2
        this.y += (this.h - this.h * sy) / 2
        this.w *= sx;
        this.h *= sy;
        return (this);
    }
    
    public get x():number { return (this._x); }
    public get y():number { return (this._y); }
    public get w():number { return (this._w); }
    public get h():number { return (this._h); }
    public get triangles():Triangle[] { return (this._triangles); }

    public set x(v:number)
    {
        this._x = v;
        this.generateTriangles();
    }
    public set y(v:number)
    {
        this._y = v;
        this.generateTriangles(); 
    }
    public set w(v:number)
    {
        this._w = v;
        this.generateTriangles(); 
    }
    public set h(v:number)
    {
        this._h = v;
        this.generateTriangles(); 
    }
    public set corner(c:number)
    {
        if (c > this.w * 0.5)
            c = this.w * 0.5;
        this._c = c;
    }
}