export class LinearMatrix<T>
{
    private data:Array<T>;
    public readonly WIDTH:number;
    public readonly HEIGHT:number;
    
    constructor(data:any)
    {
        this.data = data.data || [];
        this.WIDTH = data.width;
        this.HEIGHT = data.height;
    }

    public forEach(callback:(val:T, x:number, y:number) => void)
    {
        for (let x = 0, w = this.WIDTH; x < w; x++) {
            for (let y = 0, h = this.HEIGHT; y < h; y++) {
                callback(this.get(x, y)!, x, y);
            }
        }
    }
    public get(x:number, y:number):T|undefined
    {
        if (x >= this.WIDTH || y >= this.HEIGHT || x < 0 || y < 0)
            return (undefined);
        return (this.data[y * this.WIDTH + x]);
    }
    public set(x:number, y:number, value:T):void
    {
        this.data[y * this.WIDTH + x] = value;
    }
}