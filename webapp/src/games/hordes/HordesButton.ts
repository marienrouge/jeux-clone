import { HordesDrawer } from "./HordesDrawer";
import { HordesGame } from "./HordesGame";
import { Rectangle } from "./../lib/Rectangle";

export class HordesButton extends Rectangle
{
    private static readonly MARGIN = 5;
    public static readonly SIZE = 30;
    public static ICON_RATIO = 0.6;
    
    constructor(id:number)
    {
        super(0, 0, HordesButton.SIZE, HordesButton.SIZE);
        this.init(id);
    }

    private init(id:number)
    {
        const UI_HEIGHT = HordesGame.HEIGHT * (1 - HordesDrawer.GAME_SIZE_RATIO_HEIGHT);
        
        this.x = HordesGame.WIDTH / 2 + id * (HordesButton.SIZE + HordesButton.MARGIN);
        this.x += Math.sign(id) * HordesButton.MARGIN - HordesButton.SIZE / 2;
        this.y = HordesGame.HEIGHT - UI_HEIGHT + (UI_HEIGHT - HordesButton.SIZE) / 2;
    }
}