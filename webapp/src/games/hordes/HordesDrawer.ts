import { ADrawer } from "./../ADrawer";
import { HordesButton } from "./HordesButton";
import { HordesGame } from "./HordesGame";
import { HordesObjects } from "./HordesObjects";
import { HordesTile } from "./HordesTile";
import { LinearMatrix } from "./../lib/LinearMatrix";
import { Mouse } from "./../Mouse";
import { Vector2 } from "./../lib/Vector2";

export class HordesDrawer extends ADrawer
{
    private static readonly OBJ_NAMES = [
        "armageddon", "big_cute_cat", "call_help", "machine_gun",
        "rusty_gun", "small_hanging", "vicious_trap", "water_ration"
    ];
    private static readonly NB_BUILDING_ASSETS = 11;
    private static readonly MAX_NB_PAWNS = 7;
    private static readonly TOP_BACKGROUND_SIZE_RATIO = 0.2;
    public static readonly MARGIN = {x: 15, y: 40};
    public static readonly GAME_SIZE_RATIO_HEIGHT = 0.85;
    private scale = {x: 0, y: 0};
    private buttons:HordesButton[] = [];
    private selectedObject:number = -1;
    private imgObject:string = HordesDrawer.OBJ_NAMES[0];

    private drawButtons(objects:HordesObjects)
    {
        let clone;
        
        for (let i = this.buttons.length - 1; i >= 0; i--) {
            this.buttons[i].drawImage(this.ctx, this.getImage("object_slot")!);
            if (!objects[i])
                continue;
            if (i === this.selectedObject) {
                this.ctx.fillStyle = `rgba(255, 255, 255, ${0.1 + Math.abs(Math.cos(this.ratio)) * 0.2})`;
                this.buttons[i].draw(this.ctx, true);
            }
            clone = this.buttons[i].clone().scale(HordesButton.ICON_RATIO);
            clone.drawImage(this.ctx, this.getImage(objects[i]!)!);
        }
    }

    public init(width:number, height:number)
    {
        const OUTER_HEIGHT = HordesGame.HEIGHT * (1 - HordesDrawer.GAME_SIZE_RATIO_HEIGHT);
        
        this.scale.x = (HordesGame.WIDTH - HordesDrawer.MARGIN.x * 2) / width;
        this.scale.x = (HordesGame.WIDTH - this.scale.x / 2 - HordesDrawer.MARGIN.x * 2) / width;
        this.scale.y = (HordesGame.HEIGHT - OUTER_HEIGHT) / height;
        for (let i = -2; i <= 2; i++) {
            if (i === 0)
                continue;
            this.buttons.push(new HordesButton(i));
        }
    }
    public preload()
    {
        const BASE = `${process.env.PUBLIC_URL}/assets/hordes`;
        
        this.addImage("background", `${BASE}/background.png`);
        this.addImage("bg_bottom", `${BASE}/bg_bottom.png`);
        this.addImage("bg_top", `${BASE}/bg_top.png`);
        this.addImage("tile", `${BASE}/tile.png`);
        this.addImage("selected", `${BASE}/selected.png`);
        this.addImage("object_slot", `${BASE}/object_slot.png`);
        this.addImage("explosion", `${BASE}/explosion.png`);
        for (let i = HordesDrawer.NB_BUILDING_ASSETS; i >= 1; i--) {
            this.addImage(`building${i}`, `${BASE}/buildings/${i}.png`);
        }
        this.addImage("building0", `${BASE}/armageddon_hole.png`);
        for (let i = HordesDrawer.MAX_NB_PAWNS; i >= 1; i--) {
            this.addImage(`human${i}`, `${BASE}/humans/${i}.png`);
            this.addImage(`zombie${i}`, `${BASE}/zombies/${i}.png`);
        }
        for (const obj of HordesDrawer.OBJ_NAMES) {
            this.addImage(obj, `${BASE}/objects/${obj}.png`);
        }
    }
    public selectObject(objects?:HordesObjects):number
    {
        const isSelectionCancelled = this.buttons[this.selectedObject]?.isPointInside(Mouse.x, Mouse.y);
        
        if (!objects || (this.selectedObject !== -1 && isSelectionCancelled)) {
            this.selectedObject = -1;
            return (-1);
        }
        for (let i = this.buttons.length - 1; i >= 0; i--) {
            if (this.buttons[i].isPointInside(Mouse.x, Mouse.y)) {
                this.selectedObject = objects[i] ? i : -1;
                this.imgObject = objects[i]!;
                return (this.selectedObject);
            }
        }
        return (this.selectedObject);
    }
    public drawBackground()
    {
        let topHeight = HordesGame.HEIGHT * HordesDrawer.TOP_BACKGROUND_SIZE_RATIO;
        
        topHeight *= HordesDrawer.GAME_SIZE_RATIO_HEIGHT;
        this.drawImage("background", 0, 0, HordesGame.WIDTH, HordesGame.HEIGHT);
        this.drawImage("bg_top", 0, 0, HordesGame.WIDTH, topHeight);
        this.drawImage("bg_bottom", 0, topHeight, HordesGame.WIDTH, 
            HordesGame.HEIGHT * HordesDrawer.GAME_SIZE_RATIO_HEIGHT - topHeight
        );
    }
    public drawBoard(board:LinearMatrix<HordesTile>)
    {
        board.forEach((tile, x, y) => {
            const SEED = (y * board.WIDTH + x) % HordesDrawer.NB_BUILDING_ASSETS + 1;

            tile.scale = this.scale;
            tile.position = {x, y};
            if (!tile.canBePopulated) {
                if (tile.quantity === -1)
                    tile.drawBuilding(this.ctx, this.getImage("building0")!);
                else
                    tile.drawBuilding(this.ctx, this.getImage(`building${SEED}`)!);
                return;
            }
            tile.draw(this.ctx, this.getImage("tile")!);
            if (tile.owner)
                tile.drawPeople(this.ctx, this.getImage(`human${tile.quantity}`)!);
            else
                tile.drawPeople(this.ctx, this.getImage(`zombie${tile.quantity}`)!);
        });
    }
    public drawUi(objects:HordesObjects, cur:number, next:number)
    {
        this.drawButtons(objects);
        this.ctx.fillStyle = "antiquewhite";
        this.ctx.font = "bold italic 15px Arial";
        this.ctx.fillText(`+${cur}`, HordesGame.WIDTH / 2, HordesGame.HEIGHT - 25);
        this.ctx.font = "bold italic 12px Arial";
        this.ctx.fillText(`+${next}`, HordesGame.WIDTH / 2, HordesGame.HEIGHT - 10);
    }
    public drawSelected(board:LinearMatrix<HordesTile>, cur:number)
    {
        const pos = this.getMousePosition();
        const tile = board.get(pos.x, pos.y);
        
        cur = tile?.quantity || (this.selectedObject === -1 ? cur : 0);
        if (!tile)
            return;
        tile.draw(this.ctx, this.getImage("selected")!);
        tile.drawPeople(this.ctx, this.getImage(`human${cur}`)!);
    }
    public drawObject()
    {
        if (this.selectedObject === -1)
            return;
        this.drawImage(this.imgObject, 
            Mouse.x - HordesButton.SIZE * HordesButton.ICON_RATIO / 2, 
            Mouse.y - HordesButton.SIZE * HordesButton.ICON_RATIO / 2, 
            HordesButton.SIZE * HordesButton.ICON_RATIO
        );
    }
    public getMousePosition():Vector2
    {
        const output = {
            x: Mouse.x - HordesDrawer.MARGIN.x, 
            y: Mouse.y - HordesDrawer.MARGIN.y
        };

        output.y = ~~(output.y / (this.scale.y * (1 - HordesTile.TILE_SLOPE_RATIO)));
        output.x = ~~(output.x / this.scale.x - (output.y % 2 ? 0.5 : 0));
        return (output);
    }
}