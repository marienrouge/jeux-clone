export enum TrifusType
{
    FIRE = "fire",
    WATER = "water",
    WOOD = "wood"
};