import { TileType } from "./TileType";

export class ExpanzColor
{
    private static readonly COLORS = [
        new ExpanzColor(TileType.GRAY, [150, 150, 150]),
        new ExpanzColor(TileType.PINK, [230, 150, 150]),
        new ExpanzColor(TileType.YELLOW, [175, 175, 25]),
        new ExpanzColor(TileType.GREEN, [50, 150, 50]),
        new ExpanzColor(TileType.BLUE, [100, 100, 200])

    ];
    
    constructor(public readonly type:TileType, 
        public readonly value:Array<number>)
    {
        
    }

    public toString():string
    {
        return (`rgb(${this.value[0]}, ${this.value[1]}, ${this.value[2]})`);
    }
    
    public static get(type:TileType):ExpanzColor
    {
        return (ExpanzColor.COLORS[type]);
    }
    public static getValue(type:TileType):Array<number>
    {
        return (ExpanzColor.COLORS[type].value);
    }
}