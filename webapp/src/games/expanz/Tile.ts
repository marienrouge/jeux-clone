import { Rectangle } from "./../lib/Rectangle";
import { Vector2 } from "./../lib/Vector2";
import { ExpanzColor } from "./ExpanzColor";
import { TileType } from "./TileType";

export class Tile extends Rectangle
{
    private static readonly SHADOW_HEIGHT_RATIO = 0.5;
    private static readonly CORNER = 3;
    private static readonly PADDING = 1;
    public static size:Vector2 = {x: 0, y: 0};
    public static image?:HTMLImageElement;
    private delta:number = 0;
    public isCaptured = false;
    public isSelected = false;
    
    constructor(public readonly position:Vector2, public color:TileType)
    {
        super(position.x * Tile.size.x, position.y * Tile.size.y, Tile.size.x, Tile.size.y, Tile.CORNER);
    }
    
    private drawShadow(ctx:CanvasRenderingContext2D)
    {
        const H = Tile.size.y * Tile.SHADOW_HEIGHT_RATIO;

        ctx.fillStyle = "rgba(0, 0, 0, 0.15)";
        this.y += Tile.size.y - H;
        this.h = H;
        super.draw(ctx, true);
        ctx.fillStyle = "rgba(255, 255, 255, 0.15)";
        this.y -= Tile.size.y - H;
        super.draw(ctx, true);
        this.h = Tile.size.y;
    }
    public update(d:number)
    {
        this.delta = d;
    }
    public draw(ctx:CanvasRenderingContext2D)
    {
        ctx.fillStyle = ExpanzColor.get(this.color).toString();
        super.draw(ctx, true);
        if (this.isCaptured) {
            ctx.fillStyle = `rgba(255, 255, 255, ${0.25 * Math.abs(Math.cos(this.delta))})`;
            super.draw(ctx, true);
        }
        else if (Tile.image) {
            ctx.drawImage(
                Tile.image,
                this.x + Tile.PADDING, this.y + Tile.PADDING,
                this.w - Tile.PADDING * 2, this.h - Tile.PADDING * 2
            );
        }
        this.drawShadow(ctx);
        if (this.isSelected) {
            ctx.fillStyle = `rgba(255, 255, 255, ${0.25 + 0.5 * Math.abs(Math.cos(this.delta))})`;
            super.draw(ctx, true);
        }
    }
}