import { Clock } from "./lib/Clock";
import { ADrawer } from "./ADrawer";
import { AGameSystem } from "./AGameSystem";
import { Mouse } from "./Mouse";

type SystemCtor<S extends AGameSystem> = new (socket:SocketIOClient.Socket, data:any) => S;
type DrawHelperCtor<D extends ADrawer> = new (ctx:CanvasRenderingContext2D) => D;

export abstract class AGame<S extends AGameSystem, D extends ADrawer>
{
    public static readonly WIDTH = parseInt(process.env.REACT_APP_GAME_WIDTH || "300");
    public static readonly HEIGHT = parseInt(process.env.REACT_APP_GAME_HEIGHT || "300");
    protected readonly system:S;
    protected readonly helper:D;
    private readonly clock = new Clock();
    private readonly ctx:CanvasRenderingContext2D;
    
    constructor(
        sysctor:SystemCtor<S>,
        dhctor:DrawHelperCtor<D>, 
        socket:SocketIOClient.Socket,
        data:any
    )
    {
        const canvas = document.getElementsByTagName("canvas")[0] as HTMLCanvasElement;
        
        canvas.width = AGame.WIDTH;
        canvas.height = AGame.HEIGHT;
        Mouse.init();
        this.ctx = canvas.getContext("2d")!;
        this.system = new sysctor(socket, data);
        this.helper = new dhctor(this.ctx);
        this.helper.preload();
        window.requestAnimationFrame(this.render);
    }

    protected update(d:number):void
    {
        this.helper.update(d);
    }
    protected abstract draw(ctx:CanvasRenderingContext2D):void;
    public render = () =>
    {
        this.update(this.clock.getElapsedTime());
        this.clock.restart();
        this.draw(this.ctx);
        window.requestAnimationFrame(this.render);
    }
}