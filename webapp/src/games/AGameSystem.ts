export abstract class AGameSystem
{
    private _hasEnded:boolean = false;
    
    constructor(protected socket:SocketIOClient.Socket, data:any)
    {
        this.socket.on("update", (data:any) => this.update(data));
        this.socket.on("end", (data:any) => {
            this.end(data);
            this._hasEnded = true;
        });
    }
    protected abstract update(data:any):void;
    protected abstract end(data:any):void
    
    public emit(e:string, data:object):void
    {
        this.socket.emit(e, data);
    }

    public get hasEnded():boolean
    {
        return (this._hasEnded);
    }
}